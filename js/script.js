var jPM = {},
    pageLoaderDone = !1,
    PLUGINS_LOCALPATH = "./plugins/",
    SLIDER_REV_VERSION = "5.3.1",
    loadedFiles = [];
! function(e) {
    e.extend(e.fn, {
        themeInit: function(t) {
            var n = e(this);
            t = t || !1, n.themeSubMenus(), n.themeScrollMenus(), n.themeCustom(t);
            var a = n.themePlugins(t);
            e.each(a, function(e, n) {
                n(t)
            })
        },
        themeRefresh: function() {
            var t = e(this);
            void 0 !== t.context && null !== t.context || (t.context = t), t.themeInit(!0), void 0 !== jQuery.fn.waypoint && Waypoint.refreshAll()
        },
        themePlugins: function(t) {
            var n = e(this);
            return void 0 !== n && null !== n || (n = e(document)), $document = e(document), {
                themePluginFakeLoader: function() {
                    if (n.find("[data-toggle=page-loader]").length > 0) {
                        var t = function() {
                            jQuery("html").addClass("has-page-loader");
                            var t = jQuery("[data-toggle=page-loader]"),
                                n = {
                                    zIndex: 9999999,
                                    spinner: t.data("spinner") || "spinner6",
                                    timeToHide: 1e3
                                };
                            t.fakeLoader(n), $document.isPageLoaderDone(function() {
                                jQuery("html").removeClass("has-page-loader"), e(window).trigger("resize")
                            })
                        };
                        $document.themeLoadPlugin(["fakeLoader/fakeLoader.min.js"], ["fakeLoader/fakeLoader.css"], t)
                    }
                },
                themePluginCountTo: function() {
                    var t = n.find('[data-toggle="count-to"]');
                    if (t.length > 0) {
                        var a = function() {
                            t.each(function() {
                                var t = e(this),
                                    n = t.data("delay") || 0;
                                t.waypoint(function() {
                                    setTimeout(function() {
                                        t.countTo({
                                            onComplete: function() {
                                                t.addClass("count-to-done")
                                            },
                                            formatter: function(e, t) {
                                                var n = e.toFixed(t.decimals);
                                                return "-0" == n && (n = "0"), n
                                            }
                                        })
                                    }, n), this.destroy()
                                }, {
                                    offset: "90%"
                                })
                            })
                        };
                        $document.themeLoadPlugin(["jquery-countto/jquery.countTo.min.js"], [], function() {
                            $document.includeWaypoints(function() {
                                $document.isPageLoaderDone(a)
                            })
                        })
                    }
                },
                themePluginTyped: function() {
                    var t = n.find("[data-typed]");
                    if (t.length > 0) {
                        var a = function() {
                            t.each(function() {
                                var t = e(this),
                                    n = t.data("typed") || null,
                                    a = t.data("typed-settings") || {},
                                    i = a.delay || 0;
                                a.autoStart = !0, a.callback = function() {
                                    "" !== a.doneClass && e.each(a.doneClass, function(t, n) {
                                        e(t).addClass(n)
                                    })
                                }, "" !== n && ("object" == typeof n && (a.strings = n), t.waypoint(function() {
                                    setTimeout(function() {
                                        t.typeIt(a)
                                    }, i), this.destroy()
                                }, {
                                    offset: "100%"
                                }))
                            })
                        };
                        $document.themeLoadPlugin(["typeit/typeit.min.js"], [], function() {
                            $document.includeWaypoints(function() {
                                $document.isPageLoaderDone(a)
                            })
                        })
                    }
                },
                themePluginDropdown: function() {
                    n.find('[data-hover="dropdown"]').length > 0 && $document.themeLoadPlugin(["bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"], [], null, "append")
                },
                themePluginVide: function() {
                    var t = n.find("[data-bg-video]");
                    if (t.length > 0) {
                        var a = function() {
                            t.each(function() {
                                var t = e(this),
                                    n = t.data("bg-video") || null,
                                    a = t.data("settings") || {},
                                    i = {
                                        className: "bg-video-video"
                                    };
                                a = jQuery.extend(i, a), null !== n && t.addClass("bg-video").vide(n, a)
                            })
                        };
                        $document.themeLoadPlugin(["vide/jquery.vide.min.js"], [], a)
                    }
                },
                themePluginBootstrapSwitch: function() {
                    var e = n.find("[data-toggle=switch]");
                    if (e.length > 0) {
                        var t = function() {
                            e.bootstrapSwitch()
                        };
                        $document.themeLoadPlugin(["bootstrap-switch/bootstrap-switch.min.js"], ["bootstrap-switch/bootstrap-switch.min.css"], t)
                    }
                },
                themePluginJpanelMenu: function() {
                    var t = n.find("[data-toggle=jpanel-menu]");
                    if (t.length > 0) {
                        var a = function() {
                            var n = t,
                                a = "",
                                i = n.data("target"),
                                o = (e(i), e(window)),
                                s = e("#header .is-sticky"),
                                l = e("html"),
                                r = function(e) {
                                    return "block" === e.css("display") || "inline-block" === e.css("display")
                                };
                            jPM = jQuery.jPanelMenu({
                                menu: i,
                                direction: n.data("direction"),
                                trigger: "." + n.attr("class"),
                                excludedPanelContent: ".jpanel-menu-exclude",
                                openPosition: "280px",
                                clone: !0,
                                keepEventHandlers: !0,
                                afterOpen: function() {
                                    n.addClass("open"), l.addClass("jpanel-menu-open"), o.trigger("resize")
                                },
                                afterClose: function() {
                                    n.removeClass("open"), l.removeClass("jpanel-menu-open"), o.trigger("resize")
                                },
                                beforeOpen: function() {
                                    s.size() > 0 && (l.addClass("jpanel-menu-opening"), s.one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", function() {
                                        l.removeClass("jpanel-menu-opening")
                                    }))
                                }
                            }), o.on("debouncedresize", function() {
                                var e = r(n);
                                if (e === !0 && "on" !== a) {
                                    jPM.on();
                                    jPM.getMenu().themeRefresh(), a = "on", n.on("click.jpm", function() {
                                        return jPM.trigger(!0), !1
                                    })
                                } else e === !1 && "off" !== a && (jPM.off(), n.off("click.jpm"), a = "off")
                            }), o.trigger("resize")
                        };
                        $document.themeLoadPlugin(["jPanelMenu/jquery.jpanelmenu.min.js", "jquery.smartresize/jquery.debouncedresize.js"], [], a)
                    }
                },
                themePluginFixTo: function() {
                    var t = n.find("[data-toggle=clingify], [data-toggle=sticky]");
                    if (t.length > 0) {
                        var a = function() {
                            var n = function(e) {
                                    var t = e.data("settings") || {};
                                    t.className = "is-sticky", t.useNativeSticky = !1, e.data("stickSettings", t)
                                },
                                a = function(t, a) {
                                    n(t);
                                    var o = t.data("stickSettings"),
                                        s = o.parent || "body",
                                        l = o.persist || !1,
                                        r = o.breakpoint || !1;
                                    a = a || "init", t.addClass("sticky").fixTo(s, o), l && i(t, o), e(window).on("resize", function() {
                                        setTimeout(function() {
                                            r && (e(window).width() <= r ? (t.fixTo("destroy"), t.data("fixto-instance", "")) : "" === t.data("fixto-instance") && t.addClass("sticky").fixTo(s, t.data("stickSettings"))), l && i(t, o)
                                        }, 400)
                                    })
                                },
                                i = function(t, n) {
                                    var a = t[0].getBoundingClientRect().top;
                                    "" !== n.mind && e(n.mind).each(function(t, n) {
                                        var i = e(n);
                                        i.length > 0 && (a -= i.outerHeight())
                                    }), "" !== t.data("fixto-instance") ? t.fixTo("setOptions", {
                                        top: a
                                    }) : t.attr("style", "top: auto;")
                                };
                            t.each(function(t) {
                                a(e(this))
                            })
                        };
                        $document.themeLoadPlugin(["fixto/fixto.js"], [], a)
                    }
                },
                themePluginFlexslider: function() {
                    var t = n.find(".flexslider");
                    if (t.length > 0) {
                        var a = function() {
                            t.each(function() {
                                var t = {
                                        animation: jQuery(this).attr("data-transition"),
                                        selector: ".slides > .slide",
                                        controlNav: !0,
                                        smoothHeight: !0,
                                        start: function(e) {
                                            e.find("[data-animate-in]").each(function() {
                                                jQuery(this).css("visibility", "hidden")
                                            }), e.find(".slide-bg").each(function() {
                                                jQuery(this).css({
                                                    "background-image": "url(" + jQuery(this).data("bg-img") + ")"
                                                }), jQuery(this).css("visibility", "visible").addClass("animated").addClass(jQuery(this).data("animate-in"))
                                            }), e.find(".slide").eq(1).find("[data-animate-in]").each(function() {
                                                jQuery(this).css("visibility", "hidden"), jQuery(this).data("animate-delay") && jQuery(this).addClass(jQuery(this).data("animate-delay")), jQuery(this).data("animate-duration") && jQuery(this).addClass(jQuery(this).data("animate-duration")), jQuery(this).css("visibility", "visible").addClass("animated").addClass(jQuery(this).data("animate-in")), jQuery(this).one("webkitAnimationEnd oanimationend msAnimationEnd animationend", function() {
                                                    jQuery(this).removeClass(jQuery(this).data("animate-in"))
                                                })
                                            })
                                        },
                                        before: function(e) {
                                            e.find(".slide-bg").each(function() {
                                                jQuery(this).removeClass(jQuery(this).data("animate-in")).removeClass("animated").css("visibility", "hidden")
                                            }), e.find(".slide").eq(e.animatingTo + 1).find("[data-animate-in]").each(function() {
                                                jQuery(this).css("visibility", "hidden")
                                            })
                                        },
                                        after: function(t) {
                                            t.find(".slide").find("[data-animate-in]").each(function() {
                                                jQuery(this).css("visibility", "hidden")
                                            }), t.find(".slide").eq(t.animatingTo + 1).find("[data-animate-in]").each(function() {
                                                jQuery(this).data("animate-delay") && jQuery(this).addClass(jQuery(this).data("animate-delay")), jQuery(this).data("animate-duration") && jQuery(this).addClass(jQuery(this).data("animate-duration")), jQuery(this).css("visibility", "visible").addClass("animated").addClass(jQuery(this).data("animate-in")), jQuery(this).one("webkitAnimationEnd oanimationend msAnimationEnd animationend", function() {
                                                    jQuery(this).removeClass(jQuery(this).data("animate-in"))
                                                })
                                            }), e(window).trigger("resize")
                                        }
                                    },
                                    n = jQuery(this).attr("data-slidernav");
                                "auto" !== n && (t = e.extend({}, t, {
                                    manualControls: n + " li a",
                                    controlsContainer: ".flexslider-wrapper"
                                })), jQuery("html").addClass("has-flexslider"), jQuery(this).flexslider(t), jQuery(".flexslider").resize()
                            })
                        };
                        $document.themeLoadPlugin(["flexslider/jquery.flexslider-min.js"], ["_overrides/plugin-flexslider.min.css", "flexslider/flexslider.css"], a)
                    }
                },
                themePluginSliderRevolution: function() {
                    if ($sliderRevolutions = n.find("[data-toggle=slider-rev]"), $sliderRevolutions.length > 0) {
                        var t = function() {
                            $sliderRevolutions.each(function() {
                                var t, n = e(this),
                                    a = $document.getScriptLocation(),
                                    i = {
                                        extensions: a + "slider-revolution/js/extensions/",
                                        responsiveLevels: [1240, 1024, 778, 480],
                                        visibilityLevels: [1240, 1024, 778, 480],
                                        navigation: {
                                            arrows: {
                                                enable: !0,
                                                style: "appstrap",
                                                tmp: "",
                                                rtl: !1,
                                                hide_onleave: !1,
                                                hide_onmobile: !0,
                                                hide_under: 481,
                                                hide_over: 9999,
                                                hide_delay: 200,
                                                hide_delay_mobile: 1200,
                                                left: {
                                                    container: "slider",
                                                    h_align: "left",
                                                    v_align: "center",
                                                    h_offset: 20,
                                                    v_offset: 0
                                                },
                                                right: {
                                                    container: "slider",
                                                    h_align: "right",
                                                    v_align: "center",
                                                    h_offset: 20,
                                                    v_offset: 0
                                                }
                                            }
                                        }
                                    };
                                n.hide(), t = e.extend(i, n.data());
                                n.css("visibility", "visible").show().revolution(t)
                            })
                        };
                        $document.themeLoadPlugin(["slider-revolution/js/jquery.themepunch.tools.min.js?v=" + SLIDER_REV_VERSION, "slider-revolution/js/jquery.themepunch.revolution.min.js?v=" + SLIDER_REV_VERSION], ["_overrides/plugin-slider-revolution.min.css", "slider-revolution/css/settings.css?v=" + SLIDER_REV_VERSION], t)
                    }
                },
                themePluginBackstretch: function() {
                    var t = n.find("[data-toggle=backstretch]");
                    if (t.length > 0) {
                        var a = function() {
                            t.each(function() {
                                var t = e(this),
                                    n = jQuery,
                                    a = [],
                                    i = {
                                        fade: 750,
                                        duration: 4e3
                                    };
                                jQuery.each(t.data("backstretch-imgs").split(","), function(e, t) {
                                    a[e] = t
                                }), t.data("backstretch-target") && (n = t.data("backstretch-target"), "self" === n ? n = t : e(n).length > 0 && (n = e(n))), a && (e("html").addClass("has-backstretch"), i = e.extend({}, i, t.data()), n.backstretch(a, i), t.data("backstretch-overlay") !== !1 && (e(".backstretch").prepend('<div class="backstretch-overlay"></div>'), t.data("backstretch-overlay-opacity") && e(".backstretch").find(".backstretch-overlay").css("background", "white").fadeTo(0, t.data("backstretch-overlay-opacity"))))
                            })
                        };
                        $document.themeLoadPlugin(["backstretch/jquery.backstretch.min.js"], [], a)
                    }
                },
                themePluginFitVids: function() {
                    var t = ["iframe[src*='player.vimeo.com']", "iframe[src*='youtube.com']", "iframe[src*='youtube-nocookie.com']", "iframe[src*='kickstarter.com'][src*='video.html']", "object", "embed"];
                    if (n.find(t.join(",")).length > 0) {
                        var a = function() {
                            e("body").fitVids({
                                ignore: ".no-fitvids"
                            })
                        };
                        $document.themeLoadPlugin(["fitvidsjs/jquery.fitvids.js"], [], a)
                    }
                },
                themePluginIsotope: function() {
                    var t = n.find("[data-toggle=isotope-grid]");
                    if (t.length > 0) {
                        var a = function() {
                            t.each(function() {
                                var t = e(this),
                                    n = t.data("isotope-options"),
                                    a = t.data("isotope-filter") || null;
                                if ($document.imagesLoaded ? t.imagesLoaded(function() {
                                        t.isotope(n)
                                    }) : t.isotope(n), a) {
                                    e(a).on("click", function(n) {
                                        n.preventDefault();
                                        var a = e(this),
                                            i = a.data("isotope-fid") || null;
                                        return i && t.isotope({
                                            filter: i
                                        }), !1
                                    })
                                }
                                e("body").addClass("has-isotope")
                            })
                        };
                        $document.themeLoadPlugin(["jquery.imagesloaded/imagesloaded.pkgd.min.js", "jquery.imagesloaded/isotope.pkgd.min.js"], [], a)
                    }
                },
                themePluginHighlightJS: function() {
                    if (n.find("code").length > 0) {
                        var t = function() {
                            e("pre code").each(function(e, t) {
                                hljs.highlightBlock(t)
                            })
                        };
                        $document.themeLoadPlugin(["highlight/highlight.min.js"], ["highlight/default.min.css", "highlight/github.min.css"], t)
                    }
                },
                themePluginOwlCarousel: function() {
                    var t = n.find('[data-toggle="owl-carousel"]'),
                        a = n.find("[data-owl-carousel-thumbs]");
                    if (t.length > 0) {
                        var i = function(n) {
                            t.each(function() {
                                var t = e(this),
                                    n = t.data("owl-carousel-settings") || null;
                                t.addClass("owl-carousel").owlCarousel(n)
                            }), a.each(function() {
                                var t = e(this),
                                    n = t.find(".owl-thumb"),
                                    a = e(t.data("owl-carousel-thumbs")) || null,
                                    i = "" !== t.data("toggle") && "owl-carousel" == t.data("toggle") || !1;
                                a && (n.eq(0).addClass("active"), n.on("click", function(t) {
                                    a.trigger("to.owl.carousel", [e(this).parent().index(), 300, !0])
                                }), i && t.owlCarousel(), a.owlCarousel(), a.on("changed.owl.carousel", function(e) {
                                    var a = e.item.index;
                                    n.removeClass("active"), n.eq(a).addClass("active"), i && t.find(".owl-item").eq(a).hasClass("active") === !1 && t.trigger("to.owl.carousel", [a, 300, !0])
                                }))
                            })
                        };
                        $document.themeLoadPlugin(["OwlCarousel2/owl.carousel.min.js"], ["_overrides/plugin-owl-carousel.min.css", "OwlCarousel2/owl.carousel.min.css", "animate.css/animate.min.css"], i)
                    }
                },
                themePluginMagnificPopup: function() {
                    var t = n.find('[data-toggle="magnific-popup"]');
                    if (t.length > 0) {
                        var a = function() {
                            var n = {
                                disableOn: 0,
                                key: null,
                                midClick: !1,
                                mainClass: "mfp-fade-zoom",
                                preloader: !0,
                                focus: "",
                                closeOnContentClick: !1,
                                closeOnBgClick: !0,
                                closeBtnInside: !0,
                                showCloseBtn: !0,
                                enableEscapeKey: !0,
                                modal: !1,
                                alignTop: !1,
                                removalDelay: 300,
                                prependTo: null,
                                fixedContentPos: "auto",
                                fixedBgPos: "auto",
                                overflowY: "auto",
                                closeMarkup: '<button title="%title%" type="button" class="mfp-close">&times;</button>',
                                tClose: "Close (Esc)",
                                tLoading: "Loading...",
                                type: "image",
                                image: {
                                    titleSrc: "data-title",
                                    verticalFit: !0
                                }
                            };
                            t.each(function() {
                                var t, a = {};
                                "" !== e(this).data("magnific-popup-settings") && (a = e(this).data("magnific-popup-settings")), t = jQuery.extend(n, a), e(this).magnificPopup(t);
                                e(this).on("mfpOpen", function(t) {
                                    e.magnificPopup.instance.next = function() {
                                        var t = this;
                                        t.wrap.removeClass("mfp-image-in"), setTimeout(function() {
                                            e.magnificPopup.proto.next.call(t)
                                        }, 120)
                                    }, e.magnificPopup.instance.prev = function() {
                                        var t = this;
                                        t.wrap.removeClass("mfp-image-in"), setTimeout(function() {
                                            e.magnificPopup.proto.prev.call(t)
                                        }, 120)
                                    }
                                }).on("mfpImageLoadComplete", function() {
                                    var t = e.magnificPopup.instance;
                                    setTimeout(function() {
                                        t.wrap.addClass("mfp-image-in")
                                    }, 10)
                                })
                            })
                        };
                        $document.themeLoadPlugin(["magnific-popup/jquery.magnific-popup.min.js"], ["_overrides/plugin-magnific-popup.min.css", "magnific-popup/magnific-popup.css"], a)
                    }
                },
                themePluginZoom: function() {
                    var e = n.find("[data-img-zoom]");
                    if (e.length > 0) {
                        $document.themeLoadPlugin(["jquery-zoom/jquery.zoom.min.js"], [], themePluginZoom)
                    }
                },
                themePluginCountdown: function() {
                    var t = n.find("[data-countdown]");
                    if (t.length > 0) {
                        var a = function() {
                            t.each(function() {
                                var t = e(this),
                                    n = t.data("countdown"),
                                    a = t.data("countdown-format") || null,
                                    i = t.data("countdown-expire-text") || null;
                                t.countdown(n).on("update.countdown", function(e) {
                                    null === a && (a = "%H hrs %M mins %S secs", e.offset.totalDays > 0 && (a = "%-d day%!d " + a), e.offset.weeks > 0 && (a = "%-w week%!w " + a)), t.html(e.strftime(a))
                                }).on("finish.countdown", function(e) {
                                    i !== i && t.html(i), t.addClass("countdown-don")
                                })
                            })
                        };
                        $document.themeLoadPlugin(["jquery.countdown/jquery.countdown.min.js"], [], a)
                    }
                },
                themePluginCubePortfolio: function() {
                    var t = n.find('[data-toggle="cbp"]');
                    if (t.length > 0) {
                        var a = function() {
                            t.each(function() {
                                var t = e(this),
                                    n = t.data("settings") || {},
                                    a = {
                                        layoutMode: "mosaic",
                                        sortToPreventGaps: !0,
                                        defaultFilter: "*",
                                        animationType: "slideDelay",
                                        gapHorizontal: 2,
                                        gapVertical: 2,
                                        gridAdjustment: "responsive",
                                        mediaQueries: [{
                                            width: 1100,
                                            cols: 4
                                        }, {
                                            width: 800,
                                            cols: 3
                                        }, {
                                            width: 480,
                                            cols: 2
                                        }, {
                                            width: 0,
                                            cols: 1
                                        }],
                                        caption: "zoom",
                                        displayTypeSpeed: 100,
                                        lightboxDelegate: ".cbp-lightbox",
                                        lightboxGallery: !0,
                                        lightboxTitleSrc: "data-title",
                                        lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',
                                        singlePageInlinePosition: "top",
                                        singlePageInlineInFocus: !0,
                                        singlePageAnimation: "fade"
                                    },
                                    i = e.extend({}, a, n);
                                i.singlePageInlineCallback = function(t, n) {
                                    var a = this,
                                        i = (e(a), e(n)),
                                        o = i.data("content") || "ajax";
                                    if ("ajax" !== o && e(o).size() > 0) {
                                        var s = e(o).clone(!0, !0);
                                        s.themeRefresh(), a.content.html(""), a.content.append(s.contents()), a.cubeportfolio.$obj.trigger("updateSinglePageInlineStart.cbp"), a.singlePageInlineIsOpen.call(a)
                                    } else "ajax" == o ? e.ajax({
                                        url: t,
                                        type: "GET",
                                        dataType: "html",
                                        timeout: 3e4
                                    }).done(function(t) {
                                        var n = e(t);
                                        n.themeRefresh(), a.content.html(""), a.content.append(n), a.cubeportfolio.$obj.trigger("updateSinglePageInlineStart.cbp"), a.singlePageInlineIsOpen.call(a), $document.imagesLoaded && a.content.imagesLoaded(function() {
                                            a.content.find('[data-toggle="owl-carousel"]').on("translated.owl.carousel", function(e) {
                                                setTimeout(function() {
                                                    a.resizeSinglePageInline()
                                                }, 200)
                                            }), setTimeout(function() {
                                                a.resizeSinglePageInline()
                                            }, 1e3)
                                        })
                                    }).fail(function() {
                                        a.updateSinglePageInline("AJAX Error! Please refresh the page!")
                                    }) : a.updateSinglePageInline("Content Error! Please refresh the page!")
                                }, i.singlePageCallback = function(t, n) {
                                    var a = this;
                                    e.ajax({
                                        url: t,
                                        type: "GET",
                                        dataType: "html",
                                        timeout: 3e4
                                    }).done(function(t) {
                                        var n = e(t);
                                        n.themeRefresh();
                                        var i;
                                        a.content.addClass("cbp-popup-content").removeClass("cbp-popup-content-basic"), a.counter && (i = e(a.getCounterMarkup(a.options.singlePageCounter, a.current + 1, a.counterTotal)), a.counter.text(i.text())), a.fromAJAX = {
                                            html: n,
                                            scripts: void 0
                                        }, --a.finishOpen <= 0 && (a.wrap.addClass("cbp-popup-ready"), a.wrap.removeClass("cbp-popup-loading"), a.content.html(""), a.content.append(n), a.checkForSocialLinks(a.content), a.cubeportfolio.$obj.trigger("updateSinglePageComplete.cbp"))
                                    }).fail(function() {
                                        a.updateSinglePage("AJAX Error! Please refresh the page!")
                                    })
                                }, $document.imagesLoaded && t.imagesLoaded(function() {
                                    t.cubeportfolio(i)
                                })
                            })
                        };
                        $document.themeLoadPlugin(["jquery.imagesloaded/imagesloaded.pkgd.min.js", "cubeportfolio/jquery.cubeportfolio.js"], ["_overrides/plugin-magnific-popup.min.css", "_overrides/cubeportfolio.min.css", "cubeportfolio/cubeportfolio.css"], a)
                    }
                }
            }
        },
        themeCustom: function(t) {
            var n = e(this);
            void 0 !== n && null !== n || (n = e(document)), $document = e(document);
            var a = n.find('[data-toggle="full-height"]');
            if (a.length > 0) {
                var i = function(t, n) {
                        return "number" == typeof n ? t - n : ("string" == typeof n && e(n).length > 0 && e(n).each(function() {
                            t -= e(n).height()
                        }), t)
                    },
                    o = function() {
                        a.each(function() {
                            var t = e(this),
                                n = t.data("parent") || window,
                                a = t.data("offset") || null,
                                o = t.data("breakpoint") || null,
                                s = e(n) || null;
                            if (s) {
                                var l = s.height(),
                                    r = l;
                                a && (r = i(r, a)), o && e(window).width() <= o ? t.css("height", "auto") : t.outerHeight(r)
                            }
                        })
                    };
                o(), e(window).on("resize", function() {
                    setTimeout(function() {
                        o()
                    }, 400)
                })
            }
            var s = n.find("[data-animate]");
            if (s.length > 0) {
                var l = function() {
                    s.each(function() {
                        var e = jQuery(this),
                            t = e.data("animate"),
                            n = e.data("animate-infinite") || null,
                            a = e.data("animate-delay") || null,
                            i = e.data("animate-duration") || null,
                            o = e.data("animate-offset") || "98%";
                        null !== n && e.addClass("infinite"), null !== a && e.css({
                            "-webkit-animation-delay": a + "s",
                            "-moz-animation-delay": a + "s",
                            "animation-delay": a + "s"
                        }), null !== i && e.css({
                            "-webkit-animation-duration": i + "s",
                            "-moz-animation-duration": i + "s",
                            "animation-duration": i + "s"
                        }), e.waypoint(function() {
                            e.addClass("animated " + t).one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function() {
                                e.addClass("animated-done")
                            }), this.destroy()
                        }, {
                            offset: o
                        })
                    })
                };
                $document.includeWaypoints(function() {
                    $document.themeLoadPlugin([], ["animate.css/animate.min.css"], function() {
                        $document.isPageLoaderDone(l)
                    })
                })
            }
            n.find('[data-scroll="scroll-state"]').each(function() {
                var t = e(this),
                    n = e(document),
                    a = t.data("scroll-amount") || e(window).outerHeight();
                n.scroll(function() {
                    e(this).scrollTop() > a ? t.addClass("scroll-state-active") : t.removeClass("scroll-state-active")
                })
            }), n.find('[data-scroll="scrollax"]').each(function() {
                var t = e(this),
                    n = e(document),
                    a = e(window),
                    i = t.data("scrollax-op-ratio") || 500,
                    o = t.data("scrollax-y-ratio") || 5;
                n.scroll(function() {
                    var e = a.scrollTop();
                    t.css({
                        opacity: 1 - e / i,
                        transform: "translateY(" + (0 - e / o) + "px)"
                    })
                })
            }), n.find('[data-toggle="quantity"]').each(function() {
                var t = e(this),
                    n = t.find(".quantity-down"),
                    a = t.find(".quantity-up"),
                    i = t.find(".quantity"),
                    o = function(e) {
                        var t = parseInt(i.val());
                        "down" === e ? t -= 1 : "up" === e && (t += 1), t < 0 && (t = 0), i.val(t)
                    };
                i.length > 0 && (n.on("click", function() {
                    o("down")
                }), a.on("click", function() {
                    o("up")
                }))
            }), n.find("[data-bg-img]").each(function() {
                var t = e(this),
                    n = t.attr("style") || "";
                n += 'background-image: url("' + t.data("bg-img") + '") !important;', t.attr("style", n).addClass("bg-img")
            }), n.find("[data-css]").each(function() {
                var t = e(this),
                    n = t.data("css") || "",
                    a = t.data("css") || {},
                    i = {};
                null !== a && "object" == typeof a && (i = e.extend(n, a), t.css(i))
            });
            var r = n.find("[data-toggle=overlay]");
            if (r.length > 0 && (r.each(function() {
                    var e = jQuery(this),
                        t = e.data("target") || null;
                    e.addClass("overlay-trigger"), jQuery(t).size() > 0 && (u = jQuery(t), e.on("click", function(t) {
                        return e.toggleClass("overlay-active"), jQuery(e.data("target")).toggleClass("overlay-active"), jQuery("body").toggleClass("overlay-open"), !1
                    }))
                }), n.find('[data-dismiss="overlay"]').each(function() {
                    var t = jQuery(this),
                        n = t.data("target") || ".overlay",
                        a = jQuery('[data-toggle="overlay"][data-target="' + n + '"]') || null;
                    e(n).size() > 0 && (n = jQuery(n), t.on("click", function(e) {
                        return n.removeClass("overlay-active"), jQuery("body").removeClass("overlay-open"), a.size() > 0 ? a.removeClass("overlay-active") : jQuery('[data-toggle="overlay"]').removeClass("overlay-active"), !1
                    }))
                })), n.find("[data-url]").each(function() {
                    var t = e(this).data("url"),
                        n = function(e) {
                            var t = document.createElement("a");
                            return t.href = e, t
                        }(t);
                    e(this).addClass("clickable-element"), e(this).on("hover", function() {
                        e(this).hover(function() {
                            e(this).addClass("hovered")
                        }, function() {
                            e(this).removeClass("hovered")
                        })
                    }), e(this).find("a").on("click", function(t) {
                        e(this).attr("href") === n.href && t.preventDefault()
                    }), e(this).on("click", function() {
                        n.host !== location.host ? window.open(n.href, "_blank") : window.location = t
                    })
                }), $searchForm = n.find("[data-toggle=search-form]"), $searchForm.length > 0) {
                var d = $searchForm,
                    c = d.data("target"),
                    u = e(c);
                if (0 === u.length) return;
                u.addClass("collapse"), e("[data-toggle=search-form]").click(function() {
                    u.collapse("toggle"), e(c + " .search").focus(), d.toggleClass("open"), e("html").toggleClass("search-form-open"), e(window).trigger("resize")
                }), e("[data-toggle=search-form-close]").click(function() {
                    u.collapse("hide"), d.removeClass("open"), e("html").removeClass("search-form-open"), e(window).trigger("resize")
                })
            }
            n.find(".theme-colours a").click(function() {
                var t = e(this),
                    a = t.attr("href").replace("#", ""),
                    i = 3 * Math.floor(6 * Math.random());
                e(".theme-colours a").removeClass("active"), e(".theme-colours a." + a).addClass("active"), "green" !== a ? n.find("#colour-scheme").attr("href", "css/colour-" + a + ".css?x=" + i) : n.find("#colour-scheme").attr("href", "#")
            }), navigator.userAgent.toLowerCase().indexOf("msie") > -1 && n.find("[placeholder]").focus(function() {
                var e = jQuery(this);
                e.val() === e.attr("placeholder") && (this.originalType && (this.type = this.originalType, delete this.originalType), e.val(""), e.removeClass("placeholder"))
            }).blur(function() {
                var e = jQuery(this);
                "" === e.val() && (e.addClass("placeholder"), e.val(e.attr("placeholder")))
            }).blur();
            var f = n.find('[data-toggle="progress-bar-animated-progress"]');
            if (f.length > 0) {
                var h = function() {
                    f.each(function() {
                        var t = jQuery(this);
                        t.waypoint(function() {
                            t.css("width", function() {
                                return e(this).attr("aria-valuenow") + "%"
                            }).addClass("progress-bar-animated-progress"), this.destroy()
                        }, {
                            offset: "98%"
                        })
                    })
                };
                $document.includeWaypoints(function() {
                    f.css("width", 0), $document.isPageLoaderDone(h)
                })
            }
            n.find('[data-toggle="collapse"]').each(function() {
                var t = e(this),
                    n = t.attr("href") || t.data("target");
                t.data("parent"), e(n).length > 0 && e(n).hasClass("show") && t.addClass("show"), t.on({
                    click: function() {
                        t.toggleClass("show", !e(n).hasClass("show")), e(window).trigger("resize");
                        var a = t.find('input[type="checkbox"]');
                        a.length > 0 && a.prop("checked", !e(n).hasClass("show"))
                    }
                })
            }), n.find('[data-toggle="radio-collapse"]').each(function(t, n) {
                var a = e(n),
                    i = e(a.data("target")),
                    o = e(a.data("parent")),
                    s = a.find("input[type=radio]"),
                    l = o.find("input[type=radio]").not(s);
                s.on("change", function() {
                    s.is(":checked") ? i.collapse("show") : i.collapse("hide")
                }), s.on("click", function() {
                    l.prop("checked", !1).trigger("change")
                })
            });
            var g = n.find("[data-modal-duration]");
            if (g.length > 0) {
                var m = g,
                    p = m.data("modal-duration"),
                    v = e('<div class="modal-progress"></div>');
                m.find(".modal-content").append(v), m.on("show.bs.modal", function(e) {
                    var t = 2,
                        n = setInterval(function() {
                            v.width(t++ + "%")
                        }, p / 100);
                    setTimeout(function() {
                        m.modal("hide"), clearInterval(n)
                    }, p)
                })
            }
            var y = n.find('[data-toggle="modal-onload"]');
            y.length > 0 && y.each(function() {
                var t = e(this),
                    n = t.data("modal-delay") || null;
                null !== n ? setTimeout(function() {
                    t.modal()
                }, n) : t.modal()
            }), $document.tooltip && n.find('[data-toggle="tooltip"]').tooltip(), $document.popover && n.find('[data-toggle="popover"]').popover(), n.find("[data-page-class]").each(function() {
                n.find("html").addClass(jQuery(this).data("page-class"))
            }), n.find(".navbar-fixed-top").size() > 0 && n.find("html").addClass("has-navbar-fixed-top"), n.find('[data-toggle="show"]').each(function() {
                var t = e(this),
                    n = t.data("target"),
                    a = e(n);
                0 !== a.length && t.click(function() {
                    return a.toggleClass("show"), !1
                })
            }), n.find("[data-toggle=show-hide]").each(function() {
                var t = jQuery(this),
                    n = t.attr("data-target"),
                    a = e(n),
                    i = "show",
                    o = t.attr("data-target-state"),
                    s = t.attr("data-callback");
                0 !== a.length && ("show" === i && a.addClass("collapse"), t.click(function() {
                    void 0 !== o && o !== !1 && (i = o), void 0 === i && (i = "show"), a.hasClass("show") ? t.removeClass("show") : t.addClass("show"), a.collapse("toggle"), s && "function" == typeof s && s()
                }))
            })
        },
        isPageLoaderDone: function(e) {
            var t = jQuery('[data-toggle="page-loader"]'),
                n = function() {
                    e && "function" == typeof e && e()
                };
            0 === t.length && n();
            var a = setInterval(function() {
                "none" == t.css("display") && (pageLoaderDone = !0, clearInterval(a), n())
            }, 500)
        },
        includeWaypoints: function(e) {
            void 0 === jQuery.fn.waypoints ? $document.themeLoadPlugin(["waypoints/jquery.waypoints.min.js"], [], e) : e && "function" == typeof e && e()
        },
        themeScrollMenus: function() {
            var t = e(this),
                n = t.find('[data-toggle="scroll-link"]'),
                a = e("#header"),
                i = e("body");
            e('[data-spy="scroll"]');
            if (n.length > 0) {
                var o = function() {
                        var e = a.outerHeight();
                        return i.hasClass("header-compact-sticky") && (e -= 35), e
                    },
                    s = function(e) {
                        if ("refresh" == e) {
                            var t = i.data("bs.scrollspy");
                            t._config.offset = o(), i.data("bs.scrollspy", t), i.scrollspy("refresh")
                        } else i.scrollspy({
                            target: ".navbar-main",
                            offset: o()
                        })
                    };
                s("init"), e(window).on("resize", function() {
                    setTimeout(function() {
                        s("refresh")
                    }, 200)
                }), n.click(function() {
                    if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
                        var t = (e(this), e(this.hash));
                        return e(window).trigger("resize"),
                            function() {
                                t = t.length ? t : e("[name=" + this.hash.slice(1) + "]"), t.length && e("html, body").animate({
                                    scrollTop: t.offset().top - o() + 2
                                }, 1e3)
                            }(), !1
                    }
                })
            }
        },
        themeSubMenus: function() {
            var t = e(this),
                n = e('.dropdown-menu [data-toggle="tab"], .dropdown-menu [data-toggle="pill"]');
            n.on("click", function(t) {
                event.preventDefault(), event.stopPropagation(), e(this).tab("show")
            }), n.on("shown.bs.tab", function(t) {
                var n = e(t.relatedTarget),
                    a = e(t.target),
                    i = a.getSelector(),
                    o = n.getSelector(),
                    s = e(i),
                    l = e(o);
                s.addClass("active"), l.removeClass("active"), e(document).find('[data-target="' + i + '"]').addClass("active"), e(document).find('[data-target="' + o + '"]').removeClass("active")
            }), t.find(".dropdown-menu [data-toggle=dropdown]").on("click", function(t) {
                t.preventDefault(), t.stopPropagation(), e(this).parent().toggleClass("show")
            }), t.find(".dropdown.dropdown-persist").on({
                "shown.bs.dropdown": function() {
                    e(this).data("closable", !1)
                },
                "hide.bs.dropdown": function(t) {
                    return temp = e(this).data("closable"), e(this).data("closable", !0), temp
                }
            }), t.find(".dropdown.dropdown-persist .dropdown-menu").on({
                click: function(t) {
                    e(this).parent(".dropdown.dropdown-persist").data("closable", !1)
                }
            })
        },
        getSelector: function() {
            var t = e(this),
                n = t.data("target");
            n && "#" !== n || (n = t.attr("href") || "");
            try {
                return e(n).length > 0 ? n : null
            } catch (e) {
                return null
            }
        },
        isIE: function() {
            if (document.documentMode || /Edge/.test(navigator.userAgent)) return !0
        },
        getScriptLocation: function() {
            var t = e("body").data("plugins-localpath") || null;
            return t || PLUGINS_LOCALPATH
        },
        themeLoadPlugin: function(t, n, a, i) {
            var o = function(e) {
                return 0 === e.indexOf("http://") || 0 === e.indexOf("https://") ? e : $document.getScriptLocation() + e
            };
            if (e.ajaxPrefilter("script", function(e) {
                    e.crossDomain = !0
                }), t.length > 0) {
                var s = 0,
                    l = function(i) {
                        ++s === t.length && (e.each(n, function(t, n) {
                            if (loadedFiles[n] === n) return !0;
                            loadedFiles[n] = n, e("head").prepend('<link href="' + o(n) + '" rel="stylesheet" type="text/css" />')
                        }), a && "function" == typeof a && a())
                    };
                e.each(t, function(t, n) {
                    if (loadedFiles[n] === n) return l(), !0;
                    if (loadedFiles[n] = n, void 0 === i) {
                        var a = {
                            url: o(n),
                            dataType: "script",
                            success: l,
                            cache: !0
                        };
                        e.ajax(a)
                    } else "append" === i ? (e('script[src*="bootstrap.min.js"]').after('<script src="' + o(n) + '"></script>'), l()) : "prepend" === i ? (e('script[src*="bootstrap.min.js"]').before('<script src="' + o(n) + '"></script>'), l()) : "head" === i && (e("head").append('<script src="' + o(n) + '"></script>'), l())
                })
            } else n.length > 0 && (e.each(n, function(t, n) {
                if (loadedFiles[n] === n) return !0;
                loadedFiles[n] = n, e("head").prepend('<link href="' + o(n) + '" rel="stylesheet" type="text/css" />')
            }), a && "function" == typeof a && a())
        }
    })
}(jQuery), $(document).ready(function() {
    "use strict";
    $(document).themeInit()
});